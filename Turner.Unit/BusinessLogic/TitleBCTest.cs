﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Turner.BusinessLogic;
using Turner.Model;

namespace Turner.Unit.BusinessLogic
{
    /// <summary>
    /// Summary description for TitleBCTest
    /// </summary>
    [TestClass]
    public class TitleBCTest
    {
        public TitleBCTest()
        {
            TitleBC = new TitleBC();
        }

        private TitleBC _titleBC;

        /// <summary>
        ///Gets or sets the TitleBC 
        ///information about and functionality for the current test run.
        ///</summary>
        public TitleBC TitleBC
        {
            get
            {
                return _titleBC;
            }
            set
            {
                _titleBC = value;
            }
        }

        /// <summary>
        /// Test Get By Id method with Valid entry
        /// </summary>
        [TestMethod]
        public void ValidGetByTitleId()
        {
            Title title = TitleBC.GetById(610);
            Assert.IsNotNull(title);
        }

        /// <summary>
        /// Test Get by Id method with invalid record
        /// </summary>
        [TestMethod]
        public void InValidGetByTitleId()
        {
            Title title = TitleBC.GetById(234242);
            Assert.IsNull(title);
        }

        /// <summary>
        // Valid the Get By Name method with valid entry
        /// </summary>
        [TestMethod]
        public void ValidTitleSearch()
        {
            List<Title> titleList = TitleBC.GetByName("Casab");
            Assert.IsNotNull(titleList);
            Assert.IsTrue(titleList.Count > 0);
        }

        /// <summary>
        /// Valid the get by Name method with invalid entry
        /// </summary>
        [TestMethod]
        public void InValidTitleSearch()
        {
            List<Title> title = TitleBC.GetByName("abcdefgh1231313");
            Assert.IsNotNull(title);
            Assert.IsTrue(title.Count == 0);
        }
    }
}
